		a. Adding API Key
			1. Go to http://admin.mailchimp.com/account/api/ and create a new API Key and copy it
			2. Go to Admin CP -> Configuration -> MailChimp_Intergration (Scroll to bottom of page) -> Paste API key in 'Mailchimp API Key' field.
		b. Adding List ID
			1. Go to http://admin.mailchimp.com/lists/ and create/open a list.
			2. Click List name and Campaign defaults and copy the list ID
			3. Go to Admin CP -> Configuration -> MailChimp_Intergration (Scroll to bottom of page) -> Paste List ID in 'List ID' Field
		c. Adding our plugin to MailChimp settings (Not Needed)
			1. Go to http://admin.mailchimp.com/lists/ and create/open a list.
			2. Click List fields and *|MERGE|* tags
			3. Click Add A Field, Select Text, Change the Field Label to User name, change tag from *|<input>|* or *|MERGE2|* to *|UNAME|* or *|MERGE2|*
		d. Add Opt-In Setting
			1. Go to Admin CP -> Configuration -> MailChimp_Intergration (Scroll to bottom of page)	
			2. Select Yes
			3. Goto Next Step
		e. Customizing the PLugin Field.		
			1. Go to Admin CP -> Configuration -> Custom Profile Fields -> Ass New Profile Field
			2. Fill it all out and save the field with these considerations:
				- The plugin only supports Check Boxes and Radio buttons
				- Remember exactly what you type in the Selectable Options Box.
				- Make sure Yes is selected on the Show on Registration? Field and Viewable by All Groups.
			3. Click edit field and up in the url box it says http://<websiteurl>/admin/index.php?module=config-profile_fields&action=edit&fid=6 take note of the FID 
			4. Go to Admin CP -> Configuration -> MailChimp_Intergration (Scroll to bottom of page)
			5. Where it says Custom Field ID type the FID number from the url (2 Steps ago) in the Custom Field ID Box.
			6. Where it says Custom Field Message type the 1st option (the yes option) from the Selectable Options Box in the Custom Profile Field.
		f. Mail Chimp Server Location
			1. Go to Admin CP -> Configuration -> MailChimp_Intergration (Scroll to bottom of page)	
			2. Type the last 3-4 digits AFTER the hyphen from the API key in to the 'Location Server API Key' Field. 
				So if your API key is: 123456789-us2. Type us2.
		g. Mail Chimp Database Prefix (Not Needed, unless you changed the prefix default settings)
			1. Go to Admin CP -> Configuration -> MailChimp_Intergration (Scroll to bottom of page)	
			2. Type the database prefix in to the 'Prefix' Field. If you dont know what it is leave it.