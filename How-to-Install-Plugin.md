## How to Install Plugin
1. Copy everything in upload folder to the root of your MyBB installation.
2. Go to Admin CP -> Plugins (on sideboard) -> Click activate on MailChimp Plugin
3. Go to Admin CP -> Configuration -> MailChimp_Intergration (Scroll to bottom of page) -> Select Yes for Plugin Activation
4. Nice, Your plugin is installed! Now to configure it.